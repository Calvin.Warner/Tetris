/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Arrays;
import java.util.Random;

import javafx.scene.paint.Color;

/**
 * This should be implemented to include your game control.
 * @author pipWolfe
 */
public class TetrisGame {
    private static final String Integer = null;
	private final Tetris tetrisApp;
//	TetrisSquare square1 = new TetrisSquare(TetrisBoard board);
//	TetrisSquare square2 = null;
//	TetrisSquare square3 = null;
//	TetrisSquare square4 = null;
    TetrisPiece currentPiece;

    String move;
	int newLoc[] = new int[2];
	int[] curLoc = new int[2];




    
    /**
     * Initialize the game. Remove the example code and replace with code
     * that creates a random piece.
     * @param tetrisApp A reference to the application (use to set messages).
     * @param board A reference to the board on which Squares are drawn
     */
    public TetrisGame(Tetris tetrisApp, TetrisBoard board) {
        this.tetrisApp = tetrisApp;
        // You can use this to show the score, etc.
        tetrisApp.setMessage("Game has started!");
        makeCurrentPiece(board);
       
        
    }
    
    
    

    /**
     * Animate the game, by moving the current tetris piece down.
     * @param board 
     */
    void update(TetrisBoard board) {
    	if(!currentPiece.isOpen("Down")) {
    		board.addSquares(currentPiece);
    		board.checkBottomFull();
    		makeCurrentPiece(board);
    //	if(!board.isClear(currentPiece., y))
    		
    		
    	}
    	currentPiece.down();
        //System.out.println("updating");
    	
    }
    
    /**
     * Makes a new piece and sets it as the current piece
     * @param board
     */
    void makeCurrentPiece(TetrisBoard board) {
        currentPiece = makeRandomPiece(board);

    }
    
    
    
    /**
     * Move the current tetris piece left.
     */
    void left() {
    	currentPiece.left();
        tetrisApp.setMessage("left key was pressed!");
        System.out.println("left key was pressed!");
    }

    /**
     * Move the current tetris piece right.
     */
    void right() {
    	currentPiece.right();
		// take this code out and replace with your code
        tetrisApp.setMessage("right key was pressed!");
        System.out.println("right key was pressed!");
    }

    /**
     * Drop the current tetris piece.
     */
    void drop() {
		// take this code out and replace with your code
    	currentPiece.down();
        tetrisApp.setMessage("drop key was pressed!");
        System.out.println("drop key was pressed!");
    }

    /**
     * Rotate the current piece counter-clockwise.
     */
    void rotateLeft() {

    	currentPiece.rotateLeft();
		// take this code out and replace with your code
        tetrisApp.setMessage("rotate left key was pressed!");
        System.out.println("rotate left key was pressed!");
    }
    
    /**
     * Rotate the current piece clockwise.
     */
    void rotateRight() {
    	currentPiece.rotateRight();
		// take this code out and replace with your code
        tetrisApp.setMessage("rotate right key was pressed!");
        System.out.println("rotate right key was pressed!");
    }
    /**
     * Makes one of the seven random tetris pieces
     * @param board
     * @return a tetrisPiece
     */
    public TetrisPiece makeRandomPiece(TetrisBoard board) {
    	// create random number here
    	Random rand = new Random();
    	int value = rand.nextInt(7);
    	if (value == 0) {
    		return new TetrisPiece(0, board);
    	}else if (value == 1) {
    		return new TetrisPiece(1, board);
    	}else if (value == 2) {
    		return new TetrisPiece(2, board);
    	}else if (value == 3) {
    		return new TetrisPiece(3, board);
    	}else if (value == 4) {
    		return new TetrisPiece(4, board);
    	}else if (value == 5) {
    		return new TetrisPiece(5, board);
    	}else{
    		return new TetrisPiece(6, board);
    	}
    	
   
    	
    }
    
    
}
