/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Arrays;
import java.util.Random;

import javafx.geometry.Insets;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * A Pane in which tetris squares can be displayed.
 * 
 * @author pipWolfe
 */
public class TetrisBoard extends Pane {
	// The size of the side of a tetris square
	public static final int SQUARE_SIZE = 20;
	// The number of squares that fit on the screen in the x and y dimensions
	public static final int X_DIM_SQUARES = 15;
	public static final int Y_DIM_SQUARES = 25; // change this constant if the
	// board is too big for your screen
	private TetrisSquare[][] square = new TetrisSquare[Y_DIM_SQUARES][X_DIM_SQUARES];
	int x;
	int y;

	/**
	 * Sizes the board to hold the specified number of squares in the x and y
	 * dimensions.
	 */
	public TetrisBoard() {
		this.setPrefHeight(Y_DIM_SQUARES * SQUARE_SIZE);
		this.setPrefWidth(X_DIM_SQUARES * SQUARE_SIZE);
		BackgroundFill myBF = new BackgroundFill(Color.LIGHTGOLDENRODYELLOW, new CornerRadii(1),
				new Insets(0.0, 0.0, 0.0, 0.0));// or null for the padding
		setBackground(new Background(myBF));

	}
	/*
	 * get the squares in the tetris square 2d array
	 */
	public TetrisSquare[][] getSquares() {
		return square;
	}
	/*
	 * set the squares in the tetris square 2d array
	 */
	public void setSquares(TetrisSquare[][] square) {
		this.square = square;
	}
	/**
	 * Adds the current piece to the 2d array data structure
	 * @param currentPiece
	 */
	public void addSquares(TetrisPiece currentPiece) {
		TetrisSquare centerSquare = currentPiece.getCenterSquare();
		TetrisSquare square1 = currentPiece.getSquare1();
		TetrisSquare square2 = currentPiece.getSquare2();
		TetrisSquare square3 = currentPiece.getSquare3();

		square[centerSquare.getY()][centerSquare.getX()] = centerSquare;
		square[square1.getY()][square1.getX()] = square1;
		square[square2.getY()][square2.getX()] = square2;
		square[square3.getY()][square3.getX()] = square3;
	}
	
	/**
	 * Checks if the location at x,y is null
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isClear(int x, int y) {
		if (square[y][x] == null) {
			return true;
		} else
			return false;
	}
	/**
	 * Checks for full rows
	 * @param x
	 * @return
	 */
	public boolean checkRows(int x) {
		for (int y = 0; y < square[y].length - 1; y++) {
			while (square[x][y] == null) {
				System.out.println(square[x][y] + "..." + x + ", " + y);
				return false;
			}
		}
		return true;
	}
	/*
	 * check each row in the data structure
	 * 
	  */
	public void checkBottomFull() {
		for (int x = square.length - 1; x >= 0; x--) {
			int count = 0;
			if (isFullRow(square, x)) {
				count++;
				System.out.println("full");
				clearRow(x);
				moveDown(x);

			}
		}
	}
	/**
	 * Clear a row if it is full
	 * @param row
	 */
	private void clearRow(int row) {
		for (int y = 0; y < square[row].length; y++) {
			square[row][y].removeFromDrawing();
			square[row][y] = null;
		}
	}

	/**
	 * Move the row down the data structure and move pieces on the board down
	 * @param rowToClear
	 */
	public void moveDown(int rowToClear) {
		for (int r = rowToClear; r > 0; r--) {
			for (int c = 0; c < square[r].length; c++) {
				square[r][c] = square[r - 1][c];
				if (square[r][c] != null) {
					square[r][c].moveToTetrisLocation(c, r);
				}
			}
			System.out.println("clearing...");
		}
	}
	/**
	 * Checks each spot in the row to check if row is full
	 * @param square
	 * @param x
	 * @return
	 */
	public boolean isFullRow(TetrisSquare[][] square, int x) {
		for (int y = 0; y < square[x].length; y++) {
			if (square[x][y] == null) {
				return false;
			}
		}
		return true;
	}
}
