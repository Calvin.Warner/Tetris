import java.util.Arrays;

import javafx.scene.paint.Color;

/**
 * 
 * @author Calvin Warner TetrisPiece class creates pieces and moves them around
 *         the board.
 */
public class TetrisPiece {

	public static final int X_DIM_SQUARES = 15;
	public static final int Y_DIM_SQUARES = 25; // change this constant if the
	// board is too big for your screen
	TetrisSquare[] squares = new TetrisSquare[4];
	TetrisBoard board;
	TetrisSquare centerSquare = squares[0];
	int newLoc[][] = new int[4][2];
	String move;
	int curLoc[] = new int[2];

	/**
	 * Calls the method for the appropriate random TetrisPiece
	 * 
	 * @param pieceType
	 * @param board
	 */
	TetrisPiece(int pieceType, TetrisBoard board) {
		this.board = board;
		if (pieceType == 0) {
			Piece1();
		} else if (pieceType == 1) {
			Piece2();
		} else if (pieceType == 2) {
			Piece3();
		} else if (pieceType == 3) {
			Piece4();
		} else if (pieceType == 4) {
			Piece5();
		} else if (pieceType == 5) {
			Piece6();
		} else if (pieceType == 6) {
			Piece7();
		}
	}

	/*
	 * Creates Piece1
	 */
	public void Piece1() {
		squares[2] = new TetrisSquare(board);
		squares[2].moveToTetrisLocation(X_DIM_SQUARES / 2, 3);
		squares[2].setColor(Color.BLUEVIOLET);
		squares[1] = new TetrisSquare(board);
		squares[1].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3);
		squares[1].setColor(Color.BLUEVIOLET);
		squares[0] = new TetrisSquare(board); // CENTER SQUARE
		squares[0].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3 + 1);
		squares[0].setColor(Color.BLUEVIOLET);
		squares[3] = new TetrisSquare(board);
		squares[3].moveToTetrisLocation((X_DIM_SQUARES / 2), 3 + 1);
		squares[3].setColor(Color.BLUEVIOLET);
	}

	/*
	 * Creates Piece2
	 */
	public void Piece2() {
		squares[2] = new TetrisSquare(board);
		squares[2].moveToTetrisLocation(X_DIM_SQUARES / 2, 3);
		squares[2].setColor(Color.AQUAMARINE);
		squares[1] = new TetrisSquare(board);
		squares[1].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3);
		squares[1].setColor(Color.AQUAMARINE);
		squares[0] = new TetrisSquare(board); // CENTER SQUARE
		squares[0].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3 + 1);
		squares[0].setColor(Color.AQUAMARINE);
		squares[3] = new TetrisSquare(board);
		squares[3].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3 + 2);
		squares[3].setColor(Color.AQUAMARINE);
	}

	/*
	 * Creates Piece3
	 */
	public void Piece3() {
		squares[2] = new TetrisSquare(board);
		squares[2].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3);
		squares[2].setColor(Color.AZURE);
		squares[1] = new TetrisSquare(board);
		squares[1].moveToTetrisLocation((X_DIM_SQUARES / 2) + 2, 3);
		squares[1].setColor(Color.AZURE);
		squares[0] = new TetrisSquare(board); // CENTER SQUARE
		squares[0].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3 + 1);
		squares[0].setColor(Color.AZURE);
		squares[3] = new TetrisSquare(board);
		squares[3].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3 + 2);
		squares[3].setColor(Color.AZURE);
	}

	/*
	 * Creates Piece4
	 */
	public void Piece4() {
		squares[2] = new TetrisSquare(board);
		squares[2].moveToTetrisLocation(X_DIM_SQUARES / 2, 3);
		squares[2].setColor(Color.BLACK);
		squares[1] = new TetrisSquare(board);
		squares[1].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3);
		squares[1].setColor(Color.BLACK);
		squares[0] = new TetrisSquare(board); // CENTER SQUARE
		squares[0].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3 + 1);
		squares[0].setColor(Color.BLACK);
		squares[3] = new TetrisSquare(board);
		squares[3].moveToTetrisLocation((X_DIM_SQUARES / 2) + 2, 3 + 1);
		squares[3].setColor(Color.BLACK);
	}

	/*
	 * Creates Piece5
	 */
	public void Piece5() {
		squares[2] = new TetrisSquare(board);
		squares[2].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3);
		squares[2].setColor(Color.BROWN);
		squares[1] = new TetrisSquare(board);
		squares[1].moveToTetrisLocation((X_DIM_SQUARES / 2) + 2, 3);
		squares[1].setColor(Color.BROWN);
		squares[0] = new TetrisSquare(board); // CENTER SQUARE
		squares[0].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3 + 1);
		squares[0].setColor(Color.BROWN);
		squares[3] = new TetrisSquare(board);
		squares[3].moveToTetrisLocation((X_DIM_SQUARES / 2), 3 + 1);
		squares[3].setColor(Color.BROWN);
	}

	/*
	 * Creates Piece6
	 */
	public void Piece6() {
		squares[1] = new TetrisSquare(board);
		squares[1].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3);
		squares[1].setColor(Color.CHARTREUSE);
		squares[0] = new TetrisSquare(board); // CENTER PIECE
		squares[0].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3 + 1);
		squares[0].setColor(Color.CHARTREUSE);
		squares[2] = new TetrisSquare(board);
		squares[2].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3 + 2);
		squares[2].setColor(Color.CHARTREUSE);
		squares[3] = new TetrisSquare(board);
		squares[3].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3 + 3);
		squares[3].setColor(Color.CHARTREUSE);
	}

	/*
	 * Creates Piece7
	 */
	public void Piece7() {
		squares[1] = new TetrisSquare(board);
		squares[1].moveToTetrisLocation((X_DIM_SQUARES / 2), 3 + 1);
		squares[1].setColor(Color.DARKOLIVEGREEN);
		squares[0] = new TetrisSquare(board); // CENTER PIECE
		squares[0].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3 + 1);
		squares[0].setColor(Color.DARKOLIVEGREEN);
		squares[2] = new TetrisSquare(board);
		squares[2].moveToTetrisLocation((X_DIM_SQUARES / 2) + 2, 3 + 1);
		squares[2].setColor(Color.DARKOLIVEGREEN);
		squares[3] = new TetrisSquare(board);
		squares[3].moveToTetrisLocation((X_DIM_SQUARES / 2) + 1, 3 + 2);
		squares[3].setColor(Color.DARKOLIVEGREEN);
	}

	/*
	 * Moves pieces left
	 */
	void left() {
		if (isOpen("Left")) {
			for (int i = 0; i < squares.length; i++) {
				squares[i].moveToTetrisLocation(squares[i].getX() - 1, squares[i].getY());
			}
		}
	}


	/*
	 * Moves pieces right
	 */
	void right() {
		if (isOpen("Right")) {
			for (int i = 0; i < squares.length; i++) {
				squares[i].moveToTetrisLocation(squares[i].getX() + 1, squares[i].getY());
			}
		}
	}
	/*
	 * Moves pieces down
	 */
	void down() {
		if (isOpen("Down")) {
			for (int i = 0; i < squares.length; i++) {
				squares[i].moveToTetrisLocation(squares[i].getX(), squares[i].getY() + 1);
			}
		}
	}

	/*
	 * Rotate piece right
	 */
	void rotateRight() {
		if (isOpen("RtRight")) {
			for (int i = 1; i < squares.length; i++) {
				int newRelativeX = 0 - squares[i].getRelativeY(squares[0]);
				int newRelativeY = squares[i].getRelativeX(squares[0]);

				squares[i].moveToTetrisLocation(squares[0].getX() + newRelativeX, squares[0].getY() + newRelativeY);
			}
		}

	}

	/*
	 * Rotate piece left
	 */
	void rotateLeft() {
		if (isOpen("Left")) {
			for (int i = 1; i < squares.length; i++) {
				int newRelativeX = squares[i].getRelativeY(squares[0]);
				int newRelativeY = 0 - squares[i].getRelativeX(squares[0]);

				squares[i].moveToTetrisLocation(squares[0].getX() + newRelativeX, squares[0].getY() + newRelativeY);
			}
		}
	}
	
	/**
	 * Calculate the new location based on which move will be made.
	 * @param move
	 */
	public void calcNewLoc(String move) {
		switch (move) {

		case "Left":
				for (int i = 0; i < squares.length; i++){
				newLoc[i] = new int[]{(squares[i].getX() - 1),(squares[i].getY())};
				 System.out.println(Arrays.toString(newLoc[i]));			

				}
			break;
		case "Right":
				for (int i = 0; i < squares.length; i++){
				newLoc[i] = new int[]{(squares[i].getX() + 1),(squares[i].getY())};
				}
				 System.out.println(Arrays.toString(newLoc));
		    break;
		case "RtRight":
			
				for (int i = 0; i < squares.length; i++){
						int newRelativeX = 0 - squares[i].getRelativeY(squares[0]);
						int newRelativeY = squares[i].getRelativeX(squares[0]); 
						
				newLoc[i] = new int[] {(squares[i].getX() + newRelativeX),(squares[i].getY() + newRelativeY)};
			}
			break;
		case "RtLeft":
			for (int i = 0; i < squares.length; i++){
				int newRelativeX = squares[i].getRelativeY(squares[0]);
				int newRelativeY = 0 - squares[i].getRelativeX(squares[0]);
				
				newLoc[i] = new int[] {(squares[i].getX() + newRelativeX),(squares[i].getY() + newRelativeY)};
			}
			break;
		case "Down":
			for (int i = 0; i < squares.length; i++){
				newLoc[i] = new int[] {(squares[i].getX()), (squares[i].getY() + 1)};
			}
			break;
		}
	}
	
	/*
	 * Get the center square
	 */
	public TetrisSquare getCenterSquare() {
		return squares[0];
	}
	/*
	 * Get sqaure1
	 */
	public TetrisSquare getSquare1() {
		return squares[1];
	}
	/*
	 * Get square2
	 */
	public TetrisSquare getSquare2() {
		return squares[2];
	}
	/*
	 * Get square3
	 */
	public TetrisSquare getSquare3() {
		return squares[3];
	}
	
	/**
	 * Set the squares in the squares array
	 * @param squares
	 */
	public void setSquare(TetrisSquare[] squares) {
		this.squares = squares;
	}
	
	
	/**
	 * isOpen checks if the new location of piece is open
	 * and checks if there is another piece in the way.
	 * @param move
	 * @return
	 */
	public boolean isOpen(String move) {
		calcNewLoc(move);
	//	System.out.print(Arrays.toString(newLoc));
	for(int i = 0; i < squares.length; i++) {	
		if (newLoc[i][0] < 0 
				|| newLoc[i][0] >= X_DIM_SQUARES 
				|| newLoc[i][1] >= Y_DIM_SQUARES) {
			return false;
		}
	}
	for(int j = 0; j < squares.length; j++) {
		if(!board.isClear((squares[j].getX()), (squares[j].getY()+1  ))) {
			return false;
		}

	}
			
	 // ADD CODE TO CHECK FOR OTHER PIECES
	return true;
}
	
}
